# PayMe

### Ubiquitous Language

* Local Account - bank account that has been created in the application. Only this type of account supports balance
* Remote Account - bank account that has not been created in the application
* Transfer - operation between accounts (Local - Local , Remote - Local, Local - Remote) that change a owner of the money.
* Incoming Transfer - for a given account, deposit operation
* Outgoing Transfer - for a given account, withdraw operation
* Account number - unique id that identify bank account
* Balance - sum of all incoming and outgoing transfer for a given local account

### Running

1. Run `mvn clean verify` to build your application
2. Start application with `java -jar target/payme-1.0-SNAPSHOT.jar`


### API documentation
 
After running app goto `http://localhost:8080/admin/swagger/ui#/`

Please note that I did not test interactions with api using Swagger, I was used only for doc purpose

### DB and versioning
H2 in memory db is used

Application's db schema is supported by liquibase

Migrations history could be find in file `migrations.xml`

db is stored under `/tmp/payme` feel fee to change it in `config.yml` and each run of the application execute migration

### Testing
* Integration tests for repositories and services
* Unit test for http endpoints `*ResourceTest`
* Acceptance test for money transfering (ideally could be written in framework supported BDD like JBehave)

### Framework 

[Dropwizard](http://www.dropwizard.io/1.3.0/docs/) has been used 