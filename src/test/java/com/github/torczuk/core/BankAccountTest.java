package com.github.torczuk.core;

import com.github.torczuk.core.exception.InsufficientAccountBalance;
import com.github.torczuk.db.hibernate.HibernateAccountRepository;
import com.github.torczuk.db.hibernate.HibernateTransferRepository;
import com.github.torczuk.util.Stubs;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static com.github.torczuk.util.Stubs.accountNumber;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.assertj.core.api.Assertions.assertThat;

public class BankAccountTest {
    public static final BigDecimal THIRTY = new BigDecimal("30");
    public static final BigDecimal TWENTY_NINE = new BigDecimal("29");

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
            .addEntityClass(Account.class)
            .addEntityClass(Transfer.class)
            .build();

    private BankAccount bankAccount;
    private TransferRepository transferRepository;
    private AccountRepository accountRepository;

    private Account account1 = new Account(Stubs.name(), accountNumber());
    private Account account2 = new Account(Stubs.name(), accountNumber());

    @Before
    public void setUp() {
        transferRepository = new HibernateTransferRepository(daoTestRule.getSessionFactory());
        accountRepository = new HibernateAccountRepository(daoTestRule.getSessionFactory());
        bankAccount = new BankAccount(accountRepository, transferRepository);
    }

    @Test
    public void shouldReturnZeroAsBalanceWhenAccountDoesNotHaveAnyTransfers() {
        //given
        String theAccountNumber = accountNumber();

        //when
        BigDecimal balance = bankAccount.balance(theAccountNumber);

        //then
        assertThat(balance).isEqualTo(ZERO);
    }

    @Test
    public void shouldCalculateBalanceOfBankAccount() {
        //given
        String theAccountNumber = accountNumber();
        transferRepository.save(new Transfer(accountNumber(), theAccountNumber, TEN));
        transferRepository.save(new Transfer(accountNumber(), theAccountNumber, TEN));
        transferRepository.save(new Transfer(accountNumber(), theAccountNumber, TEN));

        //when
        BigDecimal balanceAfterDeposit = bankAccount.balance(theAccountNumber);

        //then
        assertThat(balanceAfterDeposit).isEqualTo(THIRTY);

        //when
        transferRepository.save(new Transfer(theAccountNumber, accountNumber(), ONE));
        BigDecimal balanceAfterWithdrawal = bankAccount.balance(theAccountNumber);

        //then
        assertThat(balanceAfterWithdrawal).isEqualTo(TWENTY_NINE);
    }

    @Test
    public void shouldTransferAmountOfMoneyBetweenExistingAccounts() {
        //given
        accountRepository.save(account1);
        accountRepository.save(account2);
        transferRepository.save(new Transfer(accountNumber(), account1.getNumber(), TEN));
        transferRepository.save(new Transfer(accountNumber(), account2.getNumber(), TEN));

        //when
        bankAccount.transfer(account1.getNumber(), account2.getNumber(), ONE);

        //then
        assertThat(bankAccount.balance(account1.getNumber())).isEqualTo(new BigDecimal("9"));
        assertThat(bankAccount.balance(account2.getNumber())).isEqualTo(new BigDecimal("11"));
    }

    @Test
    public void shouldNotAllowToTransferMoneyFromExistingAccountWhenBalanceIsInsufficient() {
        //given
        accountRepository.save(account1);
        transferRepository.save(new Transfer(accountNumber(), account1.getNumber(), TEN));

        //when
        expectedException.expect(InsufficientAccountBalance.class);
        expectedException.expectMessage("Insufficient account balance");
        bankAccount.transfer(account1.getNumber(), accountNumber(), THIRTY);
    }

    @Test
    public void shouldTransferMoneyFromExternalBankAccountToLocalBankAccount() {
        //given
        accountRepository.save(account1);

        //when
        bankAccount.transfer(accountNumber(), account1.getNumber(), ONE);

        //then
        assertThat(bankAccount.balance(account1.getNumber())).isEqualTo(ONE);
    }

    @Test
    public void shouldTransferMoneyFromLocalBankAccountToExternalBankAccount() {
        //given
        accountRepository.save(account1);
        bankAccount.transfer(accountNumber(), account1.getNumber(), ONE);

        //when
        bankAccount.transfer(account1.getNumber(), accountNumber(), ONE);

        //then
        assertThat(bankAccount.balance(account1.getNumber())).isEqualTo(ZERO);
    }

    @Test
    public void shouldNotAllowToTransferMoneyBetweenExternalAccounts() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("At least one account must exits");
        bankAccount.transfer(accountNumber(), accountNumber(), ONE);
    }

    @Test
    public void shouldNotAllowToTransferNegativeAmoutOfMoney() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can not withdraw negative amount");
        bankAccount.transfer(accountNumber(), accountNumber(), new BigDecimal("-10"));
    }

}