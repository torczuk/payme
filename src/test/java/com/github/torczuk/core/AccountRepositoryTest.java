package com.github.torczuk.core;

import com.github.torczuk.db.hibernate.HibernateAccountRepository;
import io.dropwizard.testing.junit.DAOTestRule;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Optional;

import static com.github.torczuk.util.Stubs.accountNumber;
import static com.github.torczuk.util.Stubs.name;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositoryTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
            .addEntityClass(Account.class)
            .build();

    private AccountRepository accountRepository;

    @Before
    public void setUp() throws Exception {
        accountRepository = new HibernateAccountRepository(daoTestRule.getSessionFactory());
    }

    @Test
    public void shouldCreateNewAccount() {
        Account account = new Account(name(), accountNumber());

        Account saved = accountRepository.save(account);

        assertThat(account).isEqualToComparingFieldByField(saved);
        assertThat(saved.getId()).isGreaterThan(0);
    }

    @Test
    public void shouldReturnEmptyOptionalWhenAccountDoesNotExist() {
        Optional<Account> nonExisting = accountRepository.getByNumber("non-existing");

        assertThat(nonExisting.isPresent()).isEqualTo(false);
    }

    @Test
    public void shouldReturnExistingAccountByNumber() {
        Account existingAccount = accountRepository.save(new Account(name(), accountNumber()));

        Optional<Account> foundAccount = accountRepository.getByNumber(existingAccount.getNumber());

        assertThat(foundAccount.get()).isEqualTo(existingAccount);
    }

    @Test
    public void shouldContainNewCreatedAccountOnListOfAllAccounts() {
        Account existingAccount = accountRepository.save(new Account(name(), accountNumber()));

        List<Account> accounts = accountRepository.all();

        assertThat(accounts).contains(existingAccount);
    }

    @Test
    public void shouldThrowExceptionWhenAccountNumberIsMissing() {
        expectedException.expect(ConstraintViolationException.class);
        accountRepository.save(new Account(name(), null));
    }

    @Test
    public void shouldThrowExceptionWhenAccountOwnerIsMissing() {
        expectedException.expect(ConstraintViolationException.class);
        accountRepository.save(new Account(null, accountNumber()));
    }
}