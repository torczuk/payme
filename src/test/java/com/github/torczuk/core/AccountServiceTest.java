package com.github.torczuk.core;

import com.github.torczuk.db.hibernate.HibernateAccountRepository;
import com.github.torczuk.util.Stubs;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountServiceTest {

    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
            .addEntityClass(Account.class)
            .build();

    private AccountIdGenerator accountIdGenerator = new AccountIdGenerator();
    private AccountRepository accountRepository;
    private AccountService accountService;

    @Before
    public void setUp() {
        accountRepository = new HibernateAccountRepository(daoTestRule.getSessionFactory());
        accountService = new AccountService(accountIdGenerator, accountRepository);
    }

    @Test
    public void shouldCreateAccountWithNumber() {
        String ownerName = Stubs.name();

        Account createdAccount = accountService.createNewAccount(ownerName);

        Account byNumber = accountRepository.getByNumber(createdAccount.getNumber()).get();
        assertThat(byNumber).isEqualTo(createdAccount);
        assertThat(createdAccount.getOwner()).isEqualTo(ownerName);
        assertThat(createdAccount.getNumber()).isNotBlank();
    }
}