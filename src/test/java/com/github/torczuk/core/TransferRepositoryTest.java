package com.github.torczuk.core;

import com.github.torczuk.db.hibernate.HibernateTransferRepository;
import io.dropwizard.testing.junit.DAOTestRule;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.List;

import static com.github.torczuk.util.Stubs.accountNumber;
import static java.math.BigDecimal.TEN;
import static org.assertj.core.api.Assertions.assertThat;

public class TransferRepositoryTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
            .addEntityClass(Transfer.class)
            .build();

    private TransferRepository transferRepository;

    @Before
    public void setUp() throws Exception {
        transferRepository = new HibernateTransferRepository(daoTestRule.getSessionFactory());
    }

    @Test
    public void shouldCreateNewTransfer() throws Exception {
        Transfer newTransfer = new Transfer(accountNumber(), accountNumber(), TEN);

        Transfer savedTransfer = transferRepository.save(newTransfer);

        assertThat(savedTransfer.getId()).isGreaterThan(0);
        assertThat(newTransfer).isEqualTo(savedTransfer);
    }

    @Test
    public void shouldReturnAllOutgoingTransfers() throws Exception {
        String accountNumber = accountNumber();
        Transfer transfer1 = transferRepository.save(new Transfer(accountNumber, accountNumber(), TEN));
        Transfer transfer2 = transferRepository.save(new Transfer(accountNumber, accountNumber(), TEN));
        Transfer transfer3 = transferRepository.save(new Transfer(accountNumber, accountNumber(), TEN));
        transferRepository.save(new Transfer(accountNumber(), accountNumber, TEN));

        List<Transfer> transfers = transferRepository.outgoingByAccountNumber(accountNumber);

        assertThat(transfers).contains(transfer1, transfer2, transfer3);
    }

    @Test
    public void shouldReturnAllIncomingTransfers() throws Exception {
        String accountNumber = accountNumber();
        Transfer transfer1 = transferRepository.save(new Transfer(accountNumber(), accountNumber, TEN));
        Transfer transfer2 = transferRepository.save(new Transfer(accountNumber(), accountNumber, TEN));
        Transfer transfer3 = transferRepository.save(new Transfer(accountNumber(), accountNumber, TEN));
        transferRepository.save(new Transfer(accountNumber, accountNumber(), TEN));

        List<Transfer> transfers = transferRepository.incomingByAccountNumber(accountNumber);

        assertThat(transfers).contains(transfer1, transfer2, transfer3);
    }

    @Test
    public void shouldThrowExceptionWhenTransferIsFromNullAccount() throws Exception {
        expectedException.expect(ConstraintViolationException.class);
        transferRepository.save(new Transfer(null, accountNumber(), BigDecimal.TEN));
    }

    @Test
    public void shouldThrowExceptionWhenTransferIsToNullAccount() throws Exception {
        expectedException.expect(ConstraintViolationException.class);
        transferRepository.save(new Transfer(accountNumber(), null, BigDecimal.TEN));
    }

    @Test
    public void shouldThrowExceptionWhenAmountToTransferIsLessThanZero() throws Exception {
        expectedException.expect(javax.validation.ConstraintViolationException.class);
        transferRepository.save(new Transfer(accountNumber(), accountNumber(), new BigDecimal("-1")));
    }

    @Test
    public void shouldThrowExceptionWhenDateOfTransferIsNull() throws Exception {
        Transfer transfer = new Transfer(accountNumber(), accountNumber(), BigDecimal.TEN);
        transfer.setDateTime(null);

        expectedException.expect(ConstraintViolationException.class);
        transferRepository.save(transfer);
    }
}
