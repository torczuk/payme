package com.github.torczuk.resources;

import com.github.torczuk.api.AccountRepresentation;
import com.github.torczuk.api.CreateAccountRequest;
import com.github.torczuk.core.Account;
import com.github.torczuk.core.AccountService;
import com.github.torczuk.core.BankAccount;
import com.github.torczuk.util.Stubs;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Optional;

import static com.github.torczuk.util.Stubs.accountNumber;
import static java.lang.String.format;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

public class AccountResourceTest {
    private static final AccountService accountService = mock(AccountService.class);
    private static final BankAccount bankAccount = mock(BankAccount.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new AccountResource(accountService, bankAccount))
            .build();

    @Before
    public void setUp() {
        reset(accountService, bankAccount);
    }

    @Test
    public void shouldCreateNewAccount() {
        String ownerName = Stubs.name();
        String accountNumber = accountNumber();
        when(accountService.createNewAccount(ownerName)).thenReturn(new Account(ownerName, accountNumber));

        Response response = resources.target("/api/v1/account").request().post(entity(new CreateAccountRequest(ownerName), APPLICATION_JSON_TYPE));

        AccountRepresentation entity = response.readEntity(AccountRepresentation.class);
        assertThat(response.getStatus()).isEqualTo(201);
        assertThat(entity.getBalance()).isEqualTo(ZERO);
        assertThat(entity.getNumber()).isEqualTo(accountNumber);
        assertThat(entity.getOwner()).isEqualTo(ownerName);
    }

    @Test
    public void shouldReturnAccountByNumber() {
        String ownerName = Stubs.name();
        String accountNumber = accountNumber();
        when(accountService.getAccountByNumber(accountNumber)).thenReturn(Optional.of(new Account(ownerName, accountNumber)));
        when(bankAccount.balance(accountNumber)).thenReturn(TEN);

        Response response = resources.target(format("/api/v1/account/%s", accountNumber)).request().get();

        AccountRepresentation entity = response.readEntity(AccountRepresentation.class);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(entity.getBalance()).isEqualTo(TEN);
        assertThat(entity.getNumber()).isEqualTo(accountNumber);
        assertThat(entity.getOwner()).isEqualTo(ownerName);
    }

    @Test
    public void shouldResponse404WhenAccountDoesNotExist() {
        when(accountService.getAccountByNumber(anyString())).thenReturn(Optional.empty());
        when(bankAccount.balance(anyString())).thenReturn(ZERO);

        Response response = resources.target(format("/api/v1/account/%s", accountNumber())).request().get();

        assertThat(response.getStatus()).isEqualTo(404);
    }
}
