package com.github.torczuk.resources;

import com.github.torczuk.api.TransferRepresentation;
import com.github.torczuk.api.TransferRequest;
import com.github.torczuk.core.Account;
import com.github.torczuk.core.AccountService;
import com.github.torczuk.core.BankAccount;
import com.github.torczuk.core.Transfer;
import com.github.torczuk.util.Stubs;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Optional;

import static com.github.torczuk.util.Stubs.accountNumber;
import static java.lang.String.format;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.util.Arrays.asList;
import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

public class TransferResourceTest {
    private static GenericType<Collection<TransferRepresentation>> TRANSFERS = new GenericType<Collection<TransferRepresentation>>() {
    };

    private static final AccountService accountService = mock(AccountService.class);
    private static final BankAccount bankAccount = mock(BankAccount.class);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(new TransferResource(accountService, bankAccount))
            .build();

    @Before
    public void setUp() {
        reset(accountService, bankAccount);
    }

    @Test
    public void shouldReturnAllTransactionsForAGivenAccount() {
        String accountNumber = accountNumber();
        Transfer transfer1 = new Transfer(accountNumber, accountNumber(), ONE);
        Transfer transfer2 = new Transfer(accountNumber(), accountNumber, TEN);
        when(accountService.getAccountByNumber(accountNumber)).thenReturn(Optional.of(new Account(Stubs.name(), accountNumber)));
        when(bankAccount.history(accountNumber)).thenReturn(asList(transfer1, transfer2));

        Response response = resources.target(format("/api/v1/account/%s/transfer", accountNumber)).request().get();

        Collection<TransferRepresentation> transfers = response.readEntity(TRANSFERS);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(transfers).hasSize(2);
    }

    @Test
    public void shouldTransferMoneyBetweenAccounts() {
        String fromAccount = Stubs.accountNumber();
        String toAccount = accountNumber();
        Transfer transfer = new Transfer(fromAccount, toAccount, TEN);
        when(accountService.getAccountByNumber(toAccount)).thenReturn(Optional.of(new Account(Stubs.name(), toAccount)));
        when(bankAccount.transfer(fromAccount, toAccount, TEN)).thenReturn(transfer);

        Response response = resources
                .target(format("/api/v1/account/%s/transfer", toAccount))
                .request()
                .post(entity(new TransferRequest(fromAccount, toAccount, TEN), APPLICATION_JSON_TYPE));

        TransferRepresentation executedTransfer = response.readEntity(TransferRepresentation.class);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(executedTransfer.getAmount()).isEqualTo(transfer.getAmount());
        assertThat(executedTransfer.getFromAccount()).isEqualTo(transfer.getFromAccountNumber());
        assertThat(executedTransfer.getToAccount()).isEqualTo(transfer.getToAccountNumber());
    }
}
