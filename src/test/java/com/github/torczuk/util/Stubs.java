package com.github.torczuk.util;

import java.util.UUID;

public class Stubs {

    public static String accountNumber() {
        return UUID.randomUUID().toString();
    }

    public static String name() {
        return UUID.randomUUID().toString().substring(0, 10);
    }
}
