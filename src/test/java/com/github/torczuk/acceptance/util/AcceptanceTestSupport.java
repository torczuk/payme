package com.github.torczuk.acceptance.util;

import com.github.torczuk.PayMe;
import com.github.torczuk.PayMeConfiguration;
import com.github.torczuk.api.AccountRepresentation;
import com.github.torczuk.api.CreateAccountRequest;
import com.github.torczuk.api.TransferRepresentation;
import com.github.torczuk.api.TransferRequest;
import io.dropwizard.db.ManagedDataSource;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.ResourceHelpers;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import static java.lang.String.format;
import static javax.ws.rs.client.Entity.json;

public class AcceptanceTestSupport {
    public static final DropwizardTestSupport<PayMeConfiguration> SUPPORT =
            new DropwizardTestSupport<>(PayMe.class,
                    ResourceHelpers.resourceFilePath("config.yml"),
                    ConfigOverride.config("server.applicationConnectors[0].port", "0")
            );
    protected static final String ACCOUNT_BY_NUMBER_ENDPOINT = "http://localhost:%d/api/v1/account/%s";
    protected static final String CREATE_ACCOUNT_ENDPOINT = "http://localhost:%d/api/v1/account";
    protected static final String TRANSFER_MONEY_ENDPOINT = "http://localhost:%d/api/v1/account/%s/transfer";

    private Client httpClient;
    private int port;

    @Before
    public void setUp() throws Exception {
        httpClient = new JerseyClientBuilder().build();
        port = SUPPORT.getLocalPort();
    }

    @BeforeClass
    public static void beforeClass() throws SQLException, LiquibaseException {
        SUPPORT.before();
        runDbMigration();
    }

    @AfterClass
    public static void afterClass() {
        SUPPORT.after();
    }

    private static void runDbMigration() throws SQLException, LiquibaseException {
        ManagedDataSource ds = SUPPORT.getConfiguration().getDataSourceFactory().build(
                SUPPORT.getEnvironment().metrics(), "migrations");

        try (Connection connection = ds.getConnection()) {
            Liquibase migrator = new Liquibase("migrations.xml", new ClassLoaderResourceAccessor(), new JdbcConnection(connection));
            migrator.update("");
        }
    }

    public TransferRepresentation transferFromExternal(String fromAccount, String toAccount, BigDecimal amount) {
        TransferRequest transferRequest = new TransferRequest(fromAccount, toAccount, amount);
        Response response = httpClient.target(format(TRANSFER_MONEY_ENDPOINT, port, toAccount)).request().post(json(transferRequest));
        return response.readEntity(TransferRepresentation.class);
    }

    public TransferRepresentation transfer(String fromAccount, String toAccount, BigDecimal amount) {
        TransferRequest transferRequest = new TransferRequest(fromAccount, toAccount, amount);
        Response response = httpClient.target(format(TRANSFER_MONEY_ENDPOINT, port, fromAccount)).request().post(json(transferRequest));
        return response.readEntity(TransferRepresentation.class);
    }

    public BigDecimal balance(AccountRepresentation account) {
        AccountRepresentation accountRepresentation = httpClient
                .target(format(ACCOUNT_BY_NUMBER_ENDPOINT, port, account.getNumber()))
                .request()
                .get(AccountRepresentation.class);
        return accountRepresentation.getBalance();
    }

    public AccountRepresentation createAccount() {
        Response response1 = httpClient
                .target(format(CREATE_ACCOUNT_ENDPOINT, port))
                .request()
                .post(json(createAccountRequest()));
        return response1.readEntity(AccountRepresentation.class);
    }


    public static CreateAccountRequest createAccountRequest() {
        return new CreateAccountRequest("new important user");
    }

    public static BigDecimal money(String value) {
        return new BigDecimal(value);
    }
}
