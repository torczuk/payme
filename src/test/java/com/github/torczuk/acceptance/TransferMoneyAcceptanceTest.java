package com.github.torczuk.acceptance;

import com.github.torczuk.acceptance.util.AcceptanceTestSupport;
import com.github.torczuk.api.AccountRepresentation;
import com.github.torczuk.util.Stubs;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferMoneyAcceptanceTest extends AcceptanceTestSupport {

    @Test
    public void shouldCreateNewLocalAccountNextExecuteTwoTransfersFirstFromExternalAccountAndNextBetweenLocalAccounts() {
        //given
        String externalAccountNumber = Stubs.accountNumber();

        //when
        AccountRepresentation account1 = createAccount();
        AccountRepresentation account2 = createAccount();

        //then
        assertThat(balance(account1)).isEqualTo(money("0"));
        assertThat(balance(account2)).isEqualTo(money("0"));

        //when
        transferFromExternal(externalAccountNumber, account1.getNumber(), money("10.00"));
        transferFromExternal(externalAccountNumber, account2.getNumber(), money("1.00"));

        //then
        assertThat(balance(account1)).isEqualTo(money("10.00"));
        assertThat(balance(account2)).isEqualTo(money("1.00"));

        //when
        transfer(account1.getNumber(), account2.getNumber(), money("5.00"));

        //then
        assertThat(balance(account1)).isEqualTo(money("5.00"));
        assertThat(balance(account2)).isEqualTo(money("6.00"));
    }


}