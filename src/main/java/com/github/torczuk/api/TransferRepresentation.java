package com.github.torczuk.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class TransferRepresentation {
    private String fromAccount;
    private String toAccount;
    private BigDecimal amount;
    private String dateTime;

    public TransferRepresentation(String fromAccount, String toAccount, BigDecimal amount, String dateTime) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
        this.dateTime = dateTime;
    }

    public TransferRepresentation() {
    }

    @JsonProperty
    public String getFromAccount() {
        return fromAccount;
    }

    @JsonProperty
    public String getToAccount() {
        return toAccount;
    }

    @JsonProperty
    public BigDecimal getAmount() {
        return amount;
    }

    @JsonProperty
    public String getDateTime() {
        return dateTime;
    }
}
