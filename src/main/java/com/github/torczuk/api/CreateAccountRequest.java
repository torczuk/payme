package com.github.torczuk.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAccountRequest {

    private String ownerName;

    public CreateAccountRequest(String ownerName) {
        this.ownerName = ownerName;
    }

    public CreateAccountRequest() {
    }

    @JsonProperty
    public String getOwnerName() {
        return ownerName;
    }
}
