package com.github.torczuk.api;

import java.math.BigDecimal;

public class AccountRepresentation {
    private String number;
    private String owner;
    private BigDecimal balance;

    public AccountRepresentation(String number, String owner, BigDecimal balance) {
        this.number = number;
        this.owner = owner;
        this.balance = balance;
    }

    public AccountRepresentation() {
    }

    public String getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    public BigDecimal getBalance() {
        return balance;
    }


}
