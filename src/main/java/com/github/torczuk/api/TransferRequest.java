package com.github.torczuk.api;

import java.math.BigDecimal;

public class TransferRequest {

    private String fromAccount;
    private String toAccount;
    private BigDecimal amount;

    public TransferRequest(String fromAccount, String toAccount, BigDecimal amount) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }

    public TransferRequest() {
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public String getToAccount() {
        return toAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
