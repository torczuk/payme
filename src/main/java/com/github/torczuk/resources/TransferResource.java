package com.github.torczuk.resources;

import com.codahale.metrics.annotation.Timed;
import com.github.torczuk.api.TransferRepresentation;
import com.github.torczuk.api.TransferRequest;
import com.github.torczuk.core.AccountService;
import com.github.torczuk.core.BankAccount;
import com.github.torczuk.core.Transfer;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.stream.Collectors;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Produces(MediaType.APPLICATION_JSON)
@Path("/api/v1/account/{accountNumber}/transfer")
@Api(value = "Transfer")
public class TransferResource {

    private final AccountService accountService;
    private final BankAccount bankAccount;

    public TransferResource(AccountService accountService, BankAccount bankAccount) {
        this.accountService = accountService;
        this.bankAccount = bankAccount;
    }

    @GET
    @Timed
    @Path("/")
    @UnitOfWork
    @ApiOperation(
            value = "account history",
            notes = "Returns all transfer that have been executed in the context of account"
    )
    public Response transfers(@PathParam("accountNumber") String accountNumber) {
        if (!accountService.getAccountByNumber(accountNumber).isPresent()) {
            return Response.status(NOT_FOUND).build();
        }
        Collection<TransferRepresentation> history = bankAccount
                .history(accountNumber)
                .stream()
                .map(transfer -> new TransferRepresentation(transfer.getFromAccountNumber(), transfer.getToAccountNumber(), transfer.getAmount(), transfer.getDateTime().toString()))
                .collect(Collectors.toList());
        return Response.ok().entity(history).build();
    }

    @POST
    @Timed
    @Path("/")
    @UnitOfWork
    @ApiOperation(
            value = "perform transfer",
            notes = "Execute transfer between two accounts. At least on of account must be local because it does not make sense to track transfers between external accounts",
            response = TransferRepresentation.class
    )
    public Response transfer(@PathParam("accountNumber") String accountNumber, TransferRequest transferRequest) {
        if (!accountService.getAccountByNumber(accountNumber).isPresent()) {
            return Response.status(NOT_FOUND).build();
        }
        Transfer transfer = bankAccount.transfer(transferRequest.getFromAccount(), transferRequest.getToAccount(), transferRequest.getAmount());
        return Response.ok().entity(new TransferRepresentation(transfer.getFromAccountNumber(), transfer.getToAccountNumber(), transfer.getAmount(), transfer.getDateTime().toString())).build();
    }
}
