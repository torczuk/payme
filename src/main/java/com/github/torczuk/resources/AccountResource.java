package com.github.torczuk.resources;


import com.codahale.metrics.annotation.Timed;
import com.github.torczuk.api.AccountRepresentation;
import com.github.torczuk.api.CreateAccountRequest;
import com.github.torczuk.core.Account;
import com.github.torczuk.core.AccountService;
import com.github.torczuk.core.BankAccount;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;


@Path("/api/v1/account")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Account")
public class AccountResource {

    private final AccountService accountService;
    private final BankAccount bankAccount;

    public AccountResource(AccountService accountService, BankAccount bankAccount) {
        this.accountService = accountService;
        this.bankAccount = bankAccount;
    }

    @POST
    @Timed
    @Path("/")
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(
            value = "crete account",
            notes = "Create new local account for a given user with name, account number is generated. All operations are executed in the context of account number",
            response = AccountRepresentation.class
    )
    public Response details(CreateAccountRequest createAccountRequest) {
        Account newAccount = accountService.createNewAccount(createAccountRequest.getOwnerName());
        AccountRepresentation representation = new AccountRepresentation(newAccount.getNumber(), newAccount.getOwner(), BigDecimal.ZERO);
        return Response
                .status(Response.Status.CREATED)
                .entity(representation).build();
    }

    @GET
    @Timed
    @Path("/{number}")
    @UnitOfWork
    @ApiOperation(
            value = "account details",
            notes = "Return basic info about local account like, owner name, account number and balance",
            response = AccountRepresentation.class
    )
    public Response details(@PathParam("number") String accountNumber) {
        return accountService.getAccountByNumber(accountNumber)
                .map(account -> {
                    BigDecimal balance = bankAccount.balance(accountNumber);
                    return Response.ok(new AccountRepresentation(accountNumber, account.getOwner(), balance)).build();
                })
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
}
