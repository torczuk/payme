package com.github.torczuk;

//import com.github.torczuk.db.AccountDAO;

import com.github.torczuk.core.Account;
import com.github.torczuk.core.AccountIdGenerator;
import com.github.torczuk.core.AccountRepository;
import com.github.torczuk.core.AccountService;
import com.github.torczuk.core.BankAccount;
import com.github.torczuk.core.Transfer;
import com.github.torczuk.core.TransferRepository;
import com.github.torczuk.db.hibernate.HibernateAccountRepository;
import com.github.torczuk.db.hibernate.HibernateTransferRepository;
import com.github.torczuk.resources.AccountResource;
import com.github.torczuk.resources.TransferResource;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.paradoxical.dropwizard.bundles.admin.AdminBundle;
import io.paradoxical.dropwizard.bundles.admin.AdminResourceEnvironment;
import io.paradoxical.dropwizard.swagger.AdminSwaggerConfiguration;
import io.paradoxical.dropwizard.swagger.AppSwaggerConfiguration;
import io.paradoxical.dropwizard.swagger.SwaggerUIConfigurator;
import io.paradoxical.dropwizard.swagger.bundles.SwaggerUIAdminAssetsBundle;
import io.paradoxical.dropwizard.swagger.bundles.SwaggerUIBundle;
import io.swagger.models.Swagger;
import io.swagger.models.auth.ApiKeyAuthDefinition;
import io.swagger.models.auth.BasicAuthDefinition;
import io.swagger.models.auth.In;

public class PayMe extends Application<PayMeConfiguration> {

    public static void main(final String[] args) throws Exception {
        PayMe payMe = new PayMe();
        payMe.run("db", "migrate", "config.yml");
        payMe.run("server", "config.yml");
    }

    @Override
    public String getName() {
        return "payme";
    }

    @Override
    public void run(final PayMeConfiguration configuration,
                    final Environment environment) {

        AccountRepository accountRepository = new HibernateAccountRepository(hibernateBundle.getSessionFactory());
        TransferRepository transferRepository = new HibernateTransferRepository(hibernateBundle.getSessionFactory());

        BankAccount bankAccount = new BankAccount(accountRepository, transferRepository);
        AccountService accountService = new AccountService(new AccountIdGenerator(), accountRepository);

        environment.jersey().register(new AccountResource(accountService, bankAccount));
        environment.jersey().register(new TransferResource(accountService, bankAccount));

    }

    private final HibernateBundle<PayMeConfiguration> hibernateBundle =
            new HibernateBundle<PayMeConfiguration>(Account.class, Transfer.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(PayMeConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public void initialize(Bootstrap<PayMeConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<PayMeConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(PayMeConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });

        bootstrap.addBundle(hibernateBundle);

        bootstrap.addBundle(
                new SwaggerUIBundle(env -> {
                    return new AppSwaggerConfiguration(env) {
                        {
                            setResourcePackage(PayMe.class.getPackage().getName());
                        }
                    };
                }));

        bootstrap.addBundle(new SwaggerUIAdminAssetsBundle());

        final SwaggerUIConfigurator adminSwaggerConfigurator = new SwaggerUIConfigurator(env -> {
            return new AdminSwaggerConfiguration("/") {
                {
                    setResourcePackage(PayMe.class.getPackage().getName());
                }
            };
        }, new Swagger() {
            {
                addSecurityDefinition("basic", new BasicAuthDefinition());
                addSecurityDefinition("token", new ApiKeyAuthDefinition("Authorization", In.HEADER));
            }
        });

        final AdminBundle adminBundle =
                AdminBundle.builder()
                        .configureEnvironment(adminSwaggerConfigurator)
                        .configureEnvironment(this::configureAdmin)
                        .build();

        bootstrap.addBundle(adminBundle);
    }

    private void configureAdmin(final Configuration configuration, final AdminResourceEnvironment adminResourceEnvironment) {
        adminResourceEnvironment.adminResourceConfig()
                .register(AccountResource.class);
    }
}

