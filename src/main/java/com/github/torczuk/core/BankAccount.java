package com.github.torczuk.core;

import com.github.torczuk.core.exception.InsufficientAccountBalance;
import io.dropwizard.hibernate.UnitOfWork;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BankAccount {

    private final AccountRepository accountRepository;
    private final TransferRepository transferRepository;

    public BankAccount(AccountRepository accountRepository, TransferRepository transferRepository) {
        this.accountRepository = accountRepository;
        this.transferRepository = transferRepository;
    }

    @UnitOfWork
    public Transfer transfer(String fromAccount, String toAccount, BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Can not withdraw negative amount");
        }
        if (isLocalBankAccount(fromAccount)) {
            BigDecimal withdrawalBalance = balance(fromAccount);
            if (!isTransferAllowed(withdrawalBalance, amount)) {
                throw new InsufficientAccountBalance("Insufficient account balance");
            }
        } else if (!isLocalBankAccount(toAccount)) {
            throw new IllegalArgumentException("At least one account must exits");
        }
        return transferRepository.save(new Transfer(fromAccount, toAccount, amount));
    }

    @UnitOfWork
    public BigDecimal balance(String accountNumber) {
        BigDecimal incoming = transferRepository
                .incomingByAccountNumber(accountNumber)
                .stream()
                .map(Transfer::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal outgoing = transferRepository
                .outgoingByAccountNumber(accountNumber)
                .stream()
                .map(Transfer::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return incoming.subtract(outgoing);
    }

    public Collection<Transfer> history(String accountNumber) {
        return Stream.concat(
                transferRepository.incomingByAccountNumber(accountNumber).stream(),
                transferRepository.outgoingByAccountNumber((accountNumber)).stream())
                .collect(Collectors.toList());
    }

    private static boolean isTransferAllowed(BigDecimal withdrawalBalance, BigDecimal amount) {
        return withdrawalBalance.subtract(amount).compareTo(BigDecimal.ZERO) >= 0;
    }

    private boolean isLocalBankAccount(String account) {
        return accountRepository.getByNumber(account).isPresent();
    }
}
