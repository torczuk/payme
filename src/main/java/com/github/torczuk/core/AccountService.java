package com.github.torczuk.core;

import java.util.Optional;

public class AccountService {

    private final AccountIdGenerator accountIdGenerator;
    private final AccountRepository accountRepository;

    public AccountService(AccountIdGenerator accountIdGenerator, AccountRepository accountRepository) {
        this.accountIdGenerator = accountIdGenerator;
        this.accountRepository = accountRepository;
    }

    public Account createNewAccount(String owner) {
        String accountNumber = accountIdGenerator.next();
        Account account = new Account(owner, accountNumber);
        return accountRepository.save(account);
    }

    public Optional<Account> getAccountByNumber(String number) {
        return accountRepository.getByNumber(number);
    }
}
