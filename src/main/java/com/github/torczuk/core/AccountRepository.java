package com.github.torczuk.core;

import java.util.List;
import java.util.Optional;

public interface AccountRepository {

    Account save(Account account);

    List<Account> all();

    Optional<Account> getByNumber(String number);
}
