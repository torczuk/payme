package com.github.torczuk.core;

import java.util.List;

public interface TransferRepository {

    Transfer save(Transfer transfer);

    List<Transfer> incomingByAccountNumber(String accountNumber);

    List<Transfer> outgoingByAccountNumber(String accountNumber);
}
