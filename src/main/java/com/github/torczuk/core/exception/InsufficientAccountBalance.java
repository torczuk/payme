package com.github.torczuk.core.exception;

public class InsufficientAccountBalance extends RuntimeException {
    public InsufficientAccountBalance() {
    }

    public InsufficientAccountBalance(String message) {
        super(message);
    }
}
