package com.github.torczuk.core;

import java.util.UUID;

public class AccountIdGenerator {
    public String next() {
        return UUID.randomUUID().toString();
    }
}
