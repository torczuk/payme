package com.github.torczuk.db.hibernate;

import com.github.torczuk.core.Transfer;
import com.github.torczuk.core.TransferRepository;
import io.dropwizard.hibernate.AbstractDAO;
import io.dropwizard.hibernate.UnitOfWork;
import org.hibernate.SessionFactory;

import java.util.List;

public class HibernateTransferRepository extends AbstractDAO<Transfer> implements TransferRepository {

    public HibernateTransferRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    @UnitOfWork
    public Transfer save(Transfer transfer) {
        return persist(transfer);
    }

    @Override
    public List<Transfer> incomingByAccountNumber(String accountNumber) {
        return query("select t from Transfer t where t.toAccountNumber = :accountNumber")
                .setParameter("accountNumber", accountNumber).list();
    }

    @Override
    public List<Transfer> outgoingByAccountNumber(String accountNumber) {
        return query("select t from Transfer t where t.fromAccountNumber = :accountNumber")
                .setParameter("accountNumber", accountNumber).list();
    }
}
