package com.github.torczuk.db.hibernate;

import com.github.torczuk.core.Account;
import com.github.torczuk.core.AccountRepository;
import io.dropwizard.hibernate.AbstractDAO;
import io.dropwizard.hibernate.UnitOfWork;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public class HibernateAccountRepository extends AbstractDAO<Account> implements AccountRepository {

    public HibernateAccountRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    @UnitOfWork
    public Account save(Account account) {
        return persist(account);
    }

    @Override
    public List<Account> all() {
        Query<Account> query = query("select a from Account a");
        return query.list();
    }

    @Override
    public Optional<Account> getByNumber(String number) {
        Query<Account> query = query("select a from Account a where a.number = :number")
                .setParameter("number", number);
        return Optional.ofNullable(uniqueResult(query));
    }
}
